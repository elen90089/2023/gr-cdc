## gr-cdc

GNU Radio out-of-tree (OOT) module for ELEN90089_2023_SM1 Communication 
Design Clinic.

This repo contains useful code for completing the CDC design project.
Current components include the following:

- CDC PHY Tx
- CDC PHY Rx

## Installation

Updates will be continuously pushed to this repo throughout semester, so it is
a good idea to make sure you have the latest version installed. If installing
on the ELEN90089 Ubuntu VM for the first time, open a terminal and enter the
following.

```
$ cd ~/code
$ git clone https://gitlab.eng.unimelb.edu.au/elen90089/2023/gr-cdc.git
$ cd gr-cdc/
```

If a clone of the repo already exists on your machine, you can pull updates
from the remote repo to get the latest changes.

```
$ git pull
```

The gr-crc OOT module is built using the standard cmake and make procedure.

```
$ mkdir build
$ cd build
$ cmake ../
$ make
$ sudo make install
$ sudo ldconfig # if this is first time installing library
```

Note the `sudo` password is that of the *cdc* Ubuntu VM account.

## Documentation

Doxygen generated documentation is available and can be view by clicking on 
the following link:

- [file:///usr/local/share/doc/gr-cdc/html/index.html](file:///usr/local/share/doc/gr-cdc/html/index.html)

or by entering the following in a terminal:

```
firefox /usr/local/share/doc/gr-cdc/html/index.html
```
