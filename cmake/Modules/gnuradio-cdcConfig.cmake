find_package(PkgConfig)

PKG_CHECK_MODULES(PC_GR_CDC gnuradio-cdc)

FIND_PATH(
    GR_CDC_INCLUDE_DIRS
    NAMES gnuradio/cdc/api.h
    HINTS $ENV{CDC_DIR}/include
        ${PC_CDC_INCLUDEDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/include
          /usr/local/include
          /usr/include
)

FIND_LIBRARY(
    GR_CDC_LIBRARIES
    NAMES gnuradio-cdc
    HINTS $ENV{CDC_DIR}/lib
        ${PC_CDC_LIBDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/lib
          ${CMAKE_INSTALL_PREFIX}/lib64
          /usr/local/lib
          /usr/local/lib64
          /usr/lib
          /usr/lib64
          )

include("${CMAKE_CURRENT_LIST_DIR}/gnuradio-cdcTarget.cmake")

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(GR_CDC DEFAULT_MSG GR_CDC_LIBRARIES GR_CDC_INCLUDE_DIRS)
MARK_AS_ADVANCED(GR_CDC_LIBRARIES GR_CDC_INCLUDE_DIRS)
