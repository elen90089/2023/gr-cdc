/* -*- c++ -*- */
/*
 * Copyright 2023 University of Melbourne.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_CDC_DSA_STATS_H
#define INCLUDED_CDC_DSA_STATS_H

#include <gnuradio/block.h>
#include <gnuradio/cdc/api.h>

namespace gr {
namespace cdc {

/*!
 * \brief Track DSA scenario statistics.
 * \ingroup cdc
 *
 * This block tracks statistics for the CDC DSA scenario. The current 
 * instantaneous and long-term average throughput are calculated based
 * on PDUs passed to the input `pdu' message port. Statistics are outputted
 * on the `stats' message port as a PMT dictionary with the following keys.
 *
 * tput_inst: instantaneous throughput (in bps)\n
 * tput_avg:  average throughput (in bps)\n
 * score:     DSA score metric
 * 
 * The score metric requires the user to set \p tput_offered, the throughput
 * offered to the primary user, and for the statistic message of the other 
 * node (whether primary or secondary) to be passed into the `stats_in'
 * message port.
 * 
 */
class CDC_API dsa_stats : virtual public gr::block {
public:
    typedef std::shared_ptr<dsa_stats> sptr;

    /*!
     * \param period        Statistic update period (in seconds)                
     * \param primary       True if this is primary user, false if secondary
     *                      user - determines how score metric is calculated
     * \param tput_offered  Offered primary throughput used in DSA score
                            calculation
     */
    static sptr make(int period = 1,
                     bool primary = true,
                     double tput_offered = 0.0);

    //! Reset DSA statistics
    virtual void reset_stats(bool reset) = 0;

    //! Set the offered primary throughput used in DSA score calculation
    virtual void set_tput_offered(double tput_offered) = 0;

    //! Get the offered primary throughput used in DSA score calculation
    virtual double get_tput_offered(void) = 0;
};

} // namespace cdc
} // namespace gr

#endif /* INCLUDED_CDC_DSA_STATS_H */
