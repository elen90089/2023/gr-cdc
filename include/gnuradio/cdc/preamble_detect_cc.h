/* -*- c++ -*- */
/*
 * Copyright 2023 University of Melbourne.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_CDC_PREAMBLE_DETECT_CC_H
#define INCLUDED_CDC_PREAMBLE_DETECT_CC_H

#include <gnuradio/cdc/api.h>
#include <gnuradio/sync_block.h>

namespace gr {
namespace cdc {

/*!
 * \brief Search for symbol sequence within sample stream via normalized
 *        correlation
 * \ingroup cdc
 *
 * Input:\n
 * Stream of complex samples.
 *
 * Output:\n
 * Output stream of input complex samples delayed by sequence length\n
 * Optional second output stream providing normalized correlator output
 * 
 * Both output streams are tagged at the beggining of the identified sequence
 * with the following stream tags. 
 *
 * corr_start: normalized correlation value for peak\n
 * corr_est:   normalized correlation value for peak\n
 * chan_est:   inverse of LLS channel estimate from training seq\n
 * phase_est:  hardcoded to zero (phase offset is handled by chan_est)\n
 * time_est:   fractional timing sample offset
 *
 * Note: 'corr_start' tag is added at the detected peak and all other tags are
 * delayed by mark_delay samples to account for possible group delay of downstream
 * matched filter block.
 *
 */
class CDC_API preamble_detect_cc : virtual public gr::sync_block {
public:
    typedef std::shared_ptr<preamble_detect_cc> sptr;

    /*!
     * \param sequence      Symbol sequence for which to search
     * \param threshold     Normalized correlation threshold to declare sequence
     *                      found (in range 0.0 to 1.0)
     * \param mark_delay    Tag marking delay to account for possible group
     *                      delay of downstream matched filter
     */
    static sptr make(const std::vector<gr_complex>& sequence,
                     float threshold = 0.5,
                     unsigned int mark_delay = 0);

    virtual std::vector<gr_complex> sequence() const = 0;
    virtual void set_sequence(const std::vector<gr_complex>& sequence) = 0;

    virtual float threshold() const = 0;
    virtual void set_threshold(float threshold) = 0;

    virtual unsigned int mark_delay(void) const = 0;
    virtual void set_mark_delay(unsigned int mark_delay) = 0;
};

} // namespace cdc
} // namespace gr

#endif /* INCLUDED_CDC_PREAMBLE_DETECT_CC_H */
