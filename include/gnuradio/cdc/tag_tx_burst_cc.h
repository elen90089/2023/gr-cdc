/* -*- c++ -*- */
/*
 * Copyright 2023 University of Melbourne.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_CDC_TAG_TX_BURST_CC_H
#define INCLUDED_CDC_TAG_TX_BURST_CC_H

#include <gnuradio/cdc/api.h>
#include <gnuradio/sync_block.h>

namespace gr {
namespace cdc {

/*!
 * \brief <+description of block+>
 * \ingroup cdc
 *
 */
class CDC_API tag_tx_burst_cc : virtual public gr::sync_block
{
public:
    typedef std::shared_ptr<tag_tx_burst_cc> sptr;

    static sptr make(const std::string &lengthtagname,
                     int scalar);
};

} // namespace cdc
} // namespace gr

#endif /* INCLUDED_CDC_TAG_TX_BURST_CC_H */
