/* -*- c++ -*- */
/*
 * Copyright 2023 University of Melbourne.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <gnuradio/io_signature.h>
#include "dsa_pu_scenario_cc_impl.h"

namespace gr {
namespace cdc {

dsa_pu_scenario_cc::sptr
dsa_pu_scenario_cc::make(int samp_rate,
                         float duration_ms,
                         bool random,
                         int seed,
                         int scenario)
{
    return gnuradio::make_block_sptr<dsa_pu_scenario_cc_impl>(
        samp_rate, duration_ms, random, seed, scenario);
}

dsa_pu_scenario_cc_impl::dsa_pu_scenario_cc_impl(int samp_rate,
                                                 float duration_ms,
                                                 bool random,
                                                 int seed,
                                                 int scenario)
    : gr::sync_block("dsa_pu_scenario_cc",
          gr::io_signature::make(1, -1, sizeof(gr_complex)),
          gr::io_signature::make(1, -1, sizeof(gr_complex))),
      d_samp_rate(samp_rate),
      d_duration_ms(duration_ms),
      d_random(random),
      d_scenario(scenario),
      d_engine(seed)
{
    message_port_register_out(pmt::mp("mode"));
}

dsa_pu_scenario_cc_impl::~dsa_pu_scenario_cc_impl()
{
    if (d_uniform) delete d_uniform;
}

void
dsa_pu_scenario_cc_impl::update_scenario(int n_chan)
{
    if (!d_active.size())
    {
        d_active.resize(n_chan);
        int n_scenarios = (0x1 << n_chan);
        d_uniform = new std::uniform_int_distribution<int>(0, n_scenarios - 1);
    }

    if (d_samp_cnt <= 0)
    {
        d_samp_cnt = int(d_duration_ms * d_samp_rate / 1000.0);
        if (d_random)
            d_scenario = (*d_uniform)(d_engine);
        message_port_pub(pmt::mp("mode"), pmt::mp(d_scenario));
    }

    for (int i_chan=0; i_chan < n_chan; i_chan++)
        d_active[i_chan] = d_scenario & (0x1 << i_chan);
}

int
dsa_pu_scenario_cc_impl::work(int noutput_items,
                              gr_vector_const_void_star &input_items,
                              gr_vector_void_star &output_items)
{
    int n_chan = output_items.size();

    update_scenario(n_chan);

    int n_samps = (d_samp_cnt < noutput_items) ? d_samp_cnt : noutput_items;
    for (int i_chan = 0; i_chan < n_chan; i_chan++)
    {
        gr_complex *in = (gr_complex *)input_items[i_chan];
        gr_complex *out = (gr_complex *)output_items[i_chan];
        if (d_active[i_chan])
            memcpy(out, in, n_samps * sizeof(gr_complex));
        else
            memset((void *)out, 0x0, n_samps * sizeof(gr_complex));
    }
    d_samp_cnt -= n_samps;

    return n_samps;
}

} /* namespace cdc */
} /* namespace gr */
