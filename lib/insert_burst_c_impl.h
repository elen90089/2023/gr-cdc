/* -*- c++ -*- */
/*
 * Copyright 2023 University of Melbourne.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_CDC_INSERT_BURST_C_IMPL_H
#define INCLUDED_CDC_INSERT_BURST_C_IMPL_H

#include <gnuradio/cdc/insert_burst_c.h>
#include <pmt/pmt.h>

namespace gr {
namespace cdc {

class insert_burst_c_impl : public insert_burst_c {
private:
    pmt::pmt_t d_burst;
    int d_length;
    int d_offset;

public:
    insert_burst_c_impl();
    ~insert_burst_c_impl();

    int work(int noutput_items,
             gr_vector_const_void_star &input_items,
             gr_vector_void_star &output_items);
};

} // namespace cdc
} // namespace gr

#endif /* INCLUDED_CDC_INSERT_BURST_C_IMPL_H */
