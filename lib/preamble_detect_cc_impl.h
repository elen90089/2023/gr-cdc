/* -*- c++ -*- */
/*
 * Copyright 2023 University of Melbourne.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_CDC_PREAMBLE_DETECT_CC_IMPL_H
#define INCLUDED_CDC_PREAMBLE_DETECT_CC_IMPL_H

#include <gnuradio/cdc/preamble_detect_cc.h>
#include <gnuradio/filter/fft_filter.h>

using namespace gr::filter;

namespace gr {
namespace cdc {

class preamble_detect_cc_impl : public preamble_detect_cc {
private:
    pmt::pmt_t d_src_id;

    std::vector<gr_complex> d_sequence;
    float d_threshold;
    unsigned int d_mark_delay;

    kernel::fft_filter_ccc d_filter;
    volk::vector<gr_complex> d_corr;
    volk::vector<float> d_corr_mag;
    volk::vector<float> d_y_mag;
    float d_scale;
    float d_y_accum;
    int d_skip;

    static constexpr int s_nitems = 24*1024;

public:
    preamble_detect_cc_impl(const std::vector<gr_complex> &sequence,
                            float threshold,
                            unsigned int mark_delay);
    ~preamble_detect_cc_impl() override;

    std::vector<gr_complex> sequence() const override;
    void set_sequence(const std::vector<gr_complex>& sequence) override;

    float threshold() const override;
    void set_threshold(float threshold) override;

    unsigned int mark_delay() const override;
    void set_mark_delay(unsigned int mark_delay) override;

    int work(int noutput_items,
             gr_vector_const_void_star &input_items,
             gr_vector_void_star &output_items);
};

} // namespace cdc
} // namespace gr

#endif /* INCLUDED_CDC_PREAMBLE_DETECT_CC_IMPL_H */
