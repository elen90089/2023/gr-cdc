/* -*- c++ -*- */
/*
 * Copyright 2023 University of Melbourne.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "tag_tx_burst_cc_impl.h"
#include <gnuradio/io_signature.h>

namespace gr {
namespace cdc {

tag_tx_burst_cc::sptr tag_tx_burst_cc::make(const std::string &lengthtagname,
                                            int scalar)
{
    return gnuradio::make_block_sptr<tag_tx_burst_cc_impl>(
        lengthtagname, scalar);
}

tag_tx_burst_cc_impl::tag_tx_burst_cc_impl(const std::string &lengthtagname,
                                           int scalar)
    : gr::sync_block(
          "tag_tx_burst_cc",
          gr::io_signature::make(1, 1, sizeof(gr_complex)),
          gr::io_signature::make(1, 1, sizeof(gr_complex))),
      d_lengthtag(pmt::mp(lengthtagname)),
      d_scalar(scalar),
      d_eob_offset(0)
{
    set_tag_propagation_policy(TPP_DONT);
}

tag_tx_burst_cc_impl::~tag_tx_burst_cc_impl() {}

int tag_tx_burst_cc_impl::work(int noutput_items,
                               gr_vector_const_void_star &input_items,
                               gr_vector_void_star &output_items) {
    auto in = static_cast<const gr_complex *>(input_items[0]);
    auto out = static_cast<gr_complex *>(output_items[0]);

    // copy input samples to output port
    memcpy(out, in, noutput_items*sizeof(gr_complex));

    // update length tag and add SOB and EOB tags
    uint64_t offset = nitems_read(0);
    uint64_t nend = nitems_read(0) + noutput_items;
    while (offset < nend) {
        if (d_eob_offset > 0) {
            if (d_eob_offset < nend) {
                offset = d_eob_offset;
                d_eob_offset = 0;
                
                add_item_tag(0,
                             offset,
                             pmt::mp("tx_eob"),
                             pmt::PMT_NIL);
            } else {
                offset = nend;
            }
        } else {
            // get tags in remaining window
            std::vector<tag_t> tags;
            get_tags_in_range(tags, 0, offset, nend, d_lengthtag);

            if (!tags.empty()) {
                long value = d_scalar * pmt::to_long(tags[0].value);
                add_item_tag(0,
                             tags[0].offset,
                             d_lengthtag,
                             pmt::from_long(value),
                             tags[0].srcid);
                add_item_tag(0,
                             tags[0].offset,
                             pmt::mp("tx_sob"),
                             pmt::PMT_NIL);
                d_eob_offset = tags[0].offset + value - 1;
            } else {
                offset = nend;
            }
        }
    }

    return noutput_items;
}

} /* namespace cdc */
} /* namespace gr */
