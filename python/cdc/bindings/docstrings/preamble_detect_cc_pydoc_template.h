/*
 * Copyright 2023 Free Software Foundation, Inc.
 *
 * This file is part of GNU Radio
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 */
#include "pydoc_macros.h"
#define D(...) DOC(gr,cdc, __VA_ARGS__ )
/*
  This file contains placeholders for docstrings for the Python bindings.
  Do not edit! These were automatically extracted during the binding process
  and will be overwritten during the build process
 */


 
 static const char *__doc_gr_cdc_preamble_detect_cc = R"doc()doc";


 static const char *__doc_gr_cdc_preamble_detect_cc_preamble_detect_cc_0 = R"doc()doc";


 static const char *__doc_gr_cdc_preamble_detect_cc_preamble_detect_cc_1 = R"doc()doc";


 static const char *__doc_gr_cdc_preamble_detect_cc_make = R"doc()doc";


 static const char *__doc_gr_cdc_preamble_detect_cc_sequence = R"doc()doc";


 static const char *__doc_gr_cdc_preamble_detect_cc_set_sequence = R"doc()doc";


 static const char *__doc_gr_cdc_preamble_detect_cc_threshold = R"doc()doc";


 static const char *__doc_gr_cdc_preamble_detect_cc_set_threshold = R"doc()doc";


 static const char *__doc_gr_cdc_preamble_detect_cc_mark_delay = R"doc()doc";


 static const char *__doc_gr_cdc_preamble_detect_cc_set_mark_delay = R"doc()doc";

  
