# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: CDC PHY Rx
# Copyright: University of Melbourne
# GNU Radio version: 3.10.1.1

from gnuradio import blocks
from gnuradio import cdc
from gnuradio import digital
from gnuradio import filter
from gnuradio import gr
from gnuradio.filter import firdes
from gnuradio.fft import window
import sys
import signal
from gnuradio import gr, pdu
import numpy as np







class phy_rx(gr.hier_block2):
    def __init__(self, excess_bw=0.350, sps=2):
        gr.hier_block2.__init__(
            self, "CDC PHY Rx",
                gr.io_signature(1, 1, gr.sizeof_gr_complex*1),
                gr.io_signature.makev(5, 5, [gr.sizeof_gr_complex*1, gr.sizeof_gr_complex*1, gr.sizeof_char*1, gr.sizeof_float*1, gr.sizeof_gr_complex*1]),
        )
        self.message_port_register_hier_out("pdu")

        ##################################################
        # Parameters
        ##################################################
        self.excess_bw = excess_bw
        self.sps = sps

        ##################################################
        # Variables
        ##################################################
        self.nfilts = nfilts = 32
        self.rrc_taps = rrc_taps = firdes.root_raised_cosine(nfilts, sps*nfilts,1.0, excess_bw, 11*sps*nfilts)
        self.modulated_sync_word = modulated_sync_word = np.array([ 6.3415498e-01+0.j, -8.2522854e-02+0.j, -6.7432624e-01+0.j, 2.1960361e-02+0.j,  6.6994393e-01+0.j,  2.7926212e-02+0.j, -6.2773204e-01+0.j, -1.6468078e-01+0.j,  5.7718420e-01+0.j, 7.2756535e-01+0.j,  4.6373290e-01+0.j,  2.3871353e-02+0.j, -4.8910391e-01+0.j, -7.3612314e-01+0.j, -4.8162562e-01+0.j, 1.2282493e-02+0.j,  4.6576929e-01+0.j,  7.3573601e-01+0.j, 5.4807413e-01+0.j, -1.3198391e-01+0.j, -5.6807721e-01+0.j, -1.1599341e-01+0.j,  6.0297143e-01+0.j,  6.1259979e-01+0.j, 3.7387580e-01+0.j,  6.1534089e-01+0.j,  6.1044985e-01+0.j, -1.3032347e-01+0.j, -5.7351917e-01+0.j, -1.1359498e-01+0.j, 5.1318854e-01+0.j,  7.6605660e-01+0.j,  5.5351597e-01+0.j, -1.7184576e-01+0.j, -6.0304552e-01+0.j, -5.2386895e-09+0.j, 6.0304564e-01+0.j,  1.7458701e-01+0.j, -5.4603779e-01+0.j, -7.7764541e-01+0.j, -5.1115209e-01+0.j,  1.2587748e-01+0.j, 5.5562639e-01+0.j,  1.4426640e-01+0.j, -5.5147964e-01+0.j, -7.4326605e-01+0.j, -4.9114031e-01+0.j,  2.7579335e-02+0.j, 5.3652304e-01+0.j,  5.9185678e-01+0.j,  4.3005973e-01+0.j, 5.5639654e-01+0.j,  5.7140875e-01+0.j,  1.4330061e-02+0.j, -5.7344508e-01+0.j, -5.8026785e-01+0.j, -4.0265229e-01+0.j, -6.0784715e-01+0.j, -6.1882788e-01+0.j,  1.3541880e-01+0.j, 6.3996756e-01+0.j, -1.1588858e-02+0.j, -6.3045293e-01+0.j, -1.2313626e-01+0.j,  5.9345692e-01+0.j,  6.3337904e-01+0.j, 4.5958614e-01+0.j,  4.4280154e-01+0.j,  4.8155165e-01+0.j, 6.0579962e-01+0.j,  5.2610868e-01+0.j,  2.5531810e-02+0.j, -4.9454585e-01+0.j, -7.3372471e-01+0.j, -5.7140863e-01+0.j, 1.6025695e-01+0.j,  6.3045293e-01+0.j, -2.2790564e-02+0.j, -6.5453744e-01+0.j, -4.3920707e-02+0.j,  6.6330218e-01+0.j, 1.3472509e-01+0.j, -6.3582081e-01+0.j, -5.8741081e-01+0.j, -4.1216698e-01+0.j, -5.9255040e-01+0.j, -5.4807413e-01+0.j, -1.3942933e-02+0.j,  5.0699669e-01+0.j,  7.0545167e-01+0.j, 5.1991683e-01+0.j, -3.5073120e-02+0.j, -5.2814502e-01+0.j, -5.9695208e-01+0.j, -4.9650821e-01+0.j, -4.3669510e-01+0.j, -4.6910083e-01+0.j, -6.3407266e-01+0.j, -5.7760048e-01+0.j, 9.9652052e-02+0.j,  5.9685379e-01+0.j,  1.1398210e-01+0.j, -5.7963693e-01+0.j, -6.4087272e-01+0.j, -4.3625149e-01+0.j, -4.7107458e-01+0.j, -5.4392737e-01+0.j, -4.6153325e-01+0.j, -4.5958611e-01+0.j, -6.1738855e-01+0.j, -5.3855950e-01+0.j, 2.7412227e-03+0.j,  5.4603767e-01+0.j,  6.0305846e-01+0.j, 4.5414424e-01+0.j,  4.7992218e-01+0.j,  5.0904173e-01+0.j, 4.9728334e-01+0.j,  5.1278079e-01+0.j,  4.7549835e-01+0.j, 4.5890158e-01+0.j,  6.0340530e-01+0.j,  5.3810960e-01+0.j, 1.3112724e-02+0.j, -5.2549899e-01+0.j, -6.6004950e-01+0.j, -4.8329139e-01+0.j, -2.2242454e-01+0.j], dtype=np.complex64)
        self.constel = constel = digital.constellation_bpsk().base()

        ##################################################
        # Blocks
        ##################################################
        self.pdu_tagged_stream_to_pdu_0 = pdu.tagged_stream_to_pdu(gr.types.byte_t, 'packet_len')
        self.digital_symbol_sync_xx_0 = digital.symbol_sync_cc(
            digital.TED_EARLY_LATE,
            sps,
            0.0157,
            1.0,
            1.0,
            1.5,
            1,
            digital.constellation_bpsk().base(),
            digital.IR_PFB_MF,
            nfilts,
            rrc_taps)
        self.digital_crc32_async_bb_0 = digital.crc32_async_bb(True)
        self.digital_costas_loop_cc_0 = digital.costas_loop_cc(0.035, constel.arity(), False)
        self.digital_correlate_access_code_xx_ts_0 = digital.correlate_access_code_bb_ts(digital.packet_utils.default_access_code,
          3, 'packet_len')
        self.digital_constellation_decoder_cb_0 = digital.constellation_decoder_cb(constel)
        self.digital_additive_scrambler_bb_0 = digital.additive_scrambler_bb(0x8A, 0x7F, 7, count=0, bits_per_byte=8, reset_tag_key="packet_len")
        self.cdc_preamble_detect_cc_0 = cdc.preamble_detect_cc(modulated_sync_word, 0.5, 0)
        self.blocks_repack_bits_bb_0 = blocks.repack_bits_bb(constel.bits_per_symbol(), 8, "packet_len", True, gr.GR_MSB_FIRST)
        self.blocks_multiply_by_tag_value_cc_0 = blocks.multiply_by_tag_value_cc('chan_est_inv', 1)


        ##################################################
        # Connections
        ##################################################
        self.msg_connect((self.digital_crc32_async_bb_0, 'out'), (self, 'pdu'))
        self.msg_connect((self.pdu_tagged_stream_to_pdu_0, 'pdus'), (self.digital_crc32_async_bb_0, 'in'))
        self.connect((self.blocks_multiply_by_tag_value_cc_0, 0), (self.digital_symbol_sync_xx_0, 0))
        self.connect((self.blocks_multiply_by_tag_value_cc_0, 0), (self, 4))
        self.connect((self.blocks_repack_bits_bb_0, 0), (self.digital_additive_scrambler_bb_0, 0))
        self.connect((self.cdc_preamble_detect_cc_0, 0), (self.blocks_multiply_by_tag_value_cc_0, 0))
        self.connect((self.cdc_preamble_detect_cc_0, 1), (self, 3))
        self.connect((self.digital_additive_scrambler_bb_0, 0), (self, 2))
        self.connect((self.digital_additive_scrambler_bb_0, 0), (self.pdu_tagged_stream_to_pdu_0, 0))
        self.connect((self.digital_constellation_decoder_cb_0, 0), (self.digital_correlate_access_code_xx_ts_0, 0))
        self.connect((self.digital_correlate_access_code_xx_ts_0, 0), (self.blocks_repack_bits_bb_0, 0))
        self.connect((self.digital_costas_loop_cc_0, 0), (self.digital_constellation_decoder_cb_0, 0))
        self.connect((self.digital_costas_loop_cc_0, 0), (self, 1))
        self.connect((self.digital_symbol_sync_xx_0, 0), (self.digital_costas_loop_cc_0, 0))
        self.connect((self.digital_symbol_sync_xx_0, 0), (self, 0))
        self.connect((self, 0), (self.cdc_preamble_detect_cc_0, 0))


    def get_excess_bw(self):
        return self.excess_bw

    def set_excess_bw(self, excess_bw):
        self.excess_bw = excess_bw
        self.set_rrc_taps(firdes.root_raised_cosine(self.nfilts, self.sps*self.nfilts, 1.0, self.excess_bw, 11*self.sps*self.nfilts))

    def get_sps(self):
        return self.sps

    def set_sps(self, sps):
        self.sps = sps
        self.set_rrc_taps(firdes.root_raised_cosine(self.nfilts, self.sps*self.nfilts, 1.0, self.excess_bw, 11*self.sps*self.nfilts))

    def get_nfilts(self):
        return self.nfilts

    def set_nfilts(self, nfilts):
        self.nfilts = nfilts
        self.set_rrc_taps(firdes.root_raised_cosine(self.nfilts, self.sps*self.nfilts, 1.0, self.excess_bw, 11*self.sps*self.nfilts))

    def get_rrc_taps(self):
        return self.rrc_taps

    def set_rrc_taps(self, rrc_taps):
        self.rrc_taps = rrc_taps

    def get_modulated_sync_word(self):
        return self.modulated_sync_word

    def set_modulated_sync_word(self, modulated_sync_word):
        self.modulated_sync_word = modulated_sync_word

    def get_constel(self):
        return self.constel

    def set_constel(self, constel):
        self.constel = constel

