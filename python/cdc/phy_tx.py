# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: CDC PHY Tx
# Copyright: University of Melbourne
# GNU Radio version: 3.10.1.1

from gnuradio import blocks
from gnuradio import digital
from gnuradio import filter
from gnuradio.filter import firdes
from gnuradio import gr
from gnuradio.fft import window
import sys
import signal
from gnuradio import gr, pdu







class phy_tx(gr.hier_block2):
    def __init__(self, excess_bw=0.350, phasing=20, sps=2):
        gr.hier_block2.__init__(
            self, "CDC PHY Tx",
                gr.io_signature(0, 0, 0),
                gr.io_signature(1, 1, gr.sizeof_gr_complex*1),
        )
        self.message_port_register_hier_in("sdu")
        self.message_port_register_hier_out("burst")

        ##################################################
        # Parameters
        ##################################################
        self.excess_bw = excess_bw
        self.phasing = phasing
        self.sps = sps

        ##################################################
        # Variables
        ##################################################
        self.constel = constel = digital.constellation_bpsk().base()
        self.access_code = access_code = digital.packet_utils.default_access_code
        self.num_taps = num_taps = 11*sps
        self.header_format = header_format = digital.header_format_default(access_code, 0, constel.bits_per_symbol())

        ##################################################
        # Blocks
        ##################################################
        self.root_raised_cosine_filter_0 = filter.interp_fir_filter_ccf(
            sps,
            firdes.root_raised_cosine(
                1.0,
                sps,
                1.0,
                excess_bw,
                num_taps))
        self.pdu_tagged_stream_to_pdu_0 = pdu.tagged_stream_to_pdu(gr.types.complex_t, 'packet_len')
        self.pdu_pdu_to_tagged_stream_0 = pdu.pdu_to_tagged_stream(gr.types.byte_t, 'packet_len')
        self.digital_protocol_formatter_bb_0 = digital.protocol_formatter_bb(header_format, "packet_len")
        self.digital_crc32_async_bb_0 = digital.crc32_async_bb(False)
        self.digital_chunks_to_symbols_xx_0 = digital.chunks_to_symbols_bc(constel.points(), 1)
        self.digital_burst_shaper_xx_0 = digital.burst_shaper_cc(firdes.window(window.WIN_HANN, phasing, 0), 0, num_taps//2, True, "packet_len")
        self.digital_additive_scrambler_bb_0 = digital.additive_scrambler_bb(0x8A, 0x7F, 7, count=0, bits_per_byte=8, reset_tag_key="packet_len")
        self.blocks_tagged_stream_mux_0 = blocks.tagged_stream_mux(gr.sizeof_char*1, 'packet_len', 0)
        self.blocks_tagged_stream_multiply_length_0 = blocks.tagged_stream_multiply_length(gr.sizeof_gr_complex*1, 'packet_len', sps)
        self.blocks_repack_bits_bb_0 = blocks.repack_bits_bb(8, constel.bits_per_symbol(), "packet_len", False, gr.GR_MSB_FIRST)


        ##################################################
        # Connections
        ##################################################
        self.msg_connect((self.digital_crc32_async_bb_0, 'out'), (self.pdu_pdu_to_tagged_stream_0, 'pdus'))
        self.msg_connect((self, 'sdu'), (self.digital_crc32_async_bb_0, 'in'))
        self.msg_connect((self.pdu_tagged_stream_to_pdu_0, 'pdus'), (self, 'burst'))
        self.connect((self.blocks_repack_bits_bb_0, 0), (self.digital_chunks_to_symbols_xx_0, 0))
        self.connect((self.blocks_tagged_stream_multiply_length_0, 0), (self.pdu_tagged_stream_to_pdu_0, 0))
        self.connect((self.blocks_tagged_stream_mux_0, 0), (self.blocks_repack_bits_bb_0, 0))
        self.connect((self.digital_additive_scrambler_bb_0, 0), (self.blocks_tagged_stream_mux_0, 1))
        self.connect((self.digital_additive_scrambler_bb_0, 0), (self.digital_protocol_formatter_bb_0, 0))
        self.connect((self.digital_burst_shaper_xx_0, 0), (self.root_raised_cosine_filter_0, 0))
        self.connect((self.digital_chunks_to_symbols_xx_0, 0), (self.digital_burst_shaper_xx_0, 0))
        self.connect((self.digital_chunks_to_symbols_xx_0, 0), (self, 0))
        self.connect((self.digital_protocol_formatter_bb_0, 0), (self.blocks_tagged_stream_mux_0, 0))
        self.connect((self.pdu_pdu_to_tagged_stream_0, 0), (self.digital_additive_scrambler_bb_0, 0))
        self.connect((self.root_raised_cosine_filter_0, 0), (self.blocks_tagged_stream_multiply_length_0, 0))


    def get_excess_bw(self):
        return self.excess_bw

    def set_excess_bw(self, excess_bw):
        self.excess_bw = excess_bw
        self.root_raised_cosine_filter_0.set_taps(firdes.root_raised_cosine(1.0, self.sps, 1.0, self.excess_bw, self.num_taps))

    def get_phasing(self):
        return self.phasing

    def set_phasing(self, phasing):
        self.phasing = phasing

    def get_sps(self):
        return self.sps

    def set_sps(self, sps):
        self.sps = sps
        self.set_num_taps(11*self.sps)
        self.blocks_tagged_stream_multiply_length_0.set_scalar(self.sps)
        self.root_raised_cosine_filter_0.set_taps(firdes.root_raised_cosine(1.0, self.sps, 1.0, self.excess_bw, self.num_taps))

    def get_constel(self):
        return self.constel

    def set_constel(self, constel):
        self.constel = constel

    def get_access_code(self):
        return self.access_code

    def set_access_code(self, access_code):
        self.access_code = access_code
        self.set_header_format(digital.header_format_default(self.access_code, 0, constel.bits_per_symbol()))

    def get_num_taps(self):
        return self.num_taps

    def set_num_taps(self, num_taps):
        self.num_taps = num_taps
        self.root_raised_cosine_filter_0.set_taps(firdes.root_raised_cosine(1.0, self.sps, 1.0, self.excess_bw, self.num_taps))

    def get_header_format(self):
        return self.header_format

    def set_header_format(self, header_format):
        self.header_format = header_format

